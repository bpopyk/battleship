#include "PrecompiledHeader.h"
#include "Canvas.h"

namespace Battleship
{
	void Canvas::SetPenColor(int r, int g, int b)
	{
		DoSetPenColor(r, g, b);
	}

	void Canvas::SetBrushColor(int r, int g, int b)
	{
		DoSetBrushColor(r, g, b);
	}

	void Canvas::DrawLine(int x1, int y1, int x2, int y2)
	{
		DoDrawLine(x1, y1, x2, y2);
	}

	void Canvas::DrawRect(int x, int y, int width, int height)
	{
		DoDrawRect(x, y, width, height);
	}

	void Canvas::DrawX(int x, int y, int width, int height)
	{
		DoDrawX(x, y, width, height);
	}

	void Canvas::BeginPaint()
	{
		DoBeginPaint();
	}

	void Canvas::EndPaint()
	{
		DoEndPaint();
	}
}