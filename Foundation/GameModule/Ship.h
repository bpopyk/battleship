#ifndef BS_SHIP_H
#define BS_SHIP_H

#include <vector>

namespace Battleship
{
	typedef std::vector<Cell*> CellsPtrContainer;

	class Ship
	{
	public:
		void AddDeck(Cell *cell);
		const Cell* CellAt(size_t offset) const;

	private:
		CellsPtrContainer mDecks;
	};
}

#endif // BS_SHIP_H