#ifndef BS_PLAYER_H
#define BS_PLAYER_H

#include "Grid.h"

namespace Battleship
{
	class Player
	{
	public:
		Player() {}
		virtual ~Player() {}

	private:
		Grid mOwnGrid, mOpponentGrid;
	};
}

#endif // BS_PLAYER_H