#include "PrecompiledHeader.h"

#include <cassert>

#include "QtCanvas.h"

namespace Battleship
{
	QtCanvas::QtCanvas(QWidget* canvasWidget) : 
		mCanvasWidget(canvasWidget), 
		mBuffer(NULL),
		mPainter(NULL)
	{
	}

	QtCanvas::~QtCanvas()
	{
		if (mPainter)
		{
			delete mPainter;
		}
	}

	void QtCanvas::AttachBufferInitPainter(QPixmap* buffer)
	{
		assert(mBuffer == NULL);
		mBuffer = buffer;
		//!!!!!!!
		mPainter = new QPainter(buffer);
	}

	void QtCanvas::DetachBufferFinalizePainter()
	{
		assert(mBuffer != NULL);
		assert(mPainter != NULL);
		delete mPainter;
		mPainter = NULL;
		mBuffer = NULL;
	}

	void QtCanvas::DoSetBrushColor(int r, int g, int b)
	{
		mBrush.setColor( qRgb(r, g, b) );
	}

	void QtCanvas::DoSetPenColor(int r, int g, int b)
	{
		mPen.setColor( qRgb(r, g, b) );
	}

	void QtCanvas::DoDrawLine(int x1, int y1, int x2, int y2)
	{
		assert(mPainter != NULL);
		mPainter->drawLine(x1, y1, x2, y2);
	}

	void QtCanvas::DoDrawRect(int x, int y, int width, int height)
	{
		assert(mPainter != NULL);
		mPainter->drawRect(x, y, width, height);
	}

	void QtCanvas::DoDrawX(int x, int y, int width, int height)
	{
		assert(mPainter != NULL);
		mPainter->drawLine(x, y, x + width, y + height);
		mPainter->drawLine(x, y + height, x + width, y);
	}

	void QtCanvas::DoBeginPaint()
	{
	}

	void QtCanvas::DoEndPaint()
	{
		assert(mCanvasWidget != NULL);
		mCanvasWidget->update();
	}
}