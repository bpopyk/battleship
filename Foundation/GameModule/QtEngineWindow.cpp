#include "PrecompiledHeader.h"
#include "QtEngineWindow.h"

namespace Battleship
{
	QtEngineWindow::QtEngineWindow(QWidget *parent) :
		QMainWindow(parent),
		mCanvas(NULL),
		mCanvasBuffer(NULL),
		mPaintingNow(false)
	{
		ui.setupUi(this);
		ReinitCanvas();
		ConnectActions();
	}

	QtEngineWindow::~QtEngineWindow()
	{
		if (mCanvas)
		{
			delete mCanvas;
			mCanvas = NULL;
		}
		if (mCanvasBuffer)
		{
			delete mCanvasBuffer;
			mCanvasBuffer = NULL;
		}
	}

	void QtEngineWindow::DoBeginPaint()
	{
		mCanvas->BeginPaint();
	}

	void QtEngineWindow::DoEndPaint()
	{
		mCanvas->EndPaint();
	}

	Canvas& QtEngineWindow::DoGetCanvas()
	{
		if ( !mCanvas )
		{
			ReinitCanvas();
		}
		return *mCanvas;
	}

	void QtEngineWindow::ReinitCanvas()
	{
		if (mCanvas)
		{
			delete mCanvas;
		}
		mCanvas = new QtCanvas(this);
		ReinitCanvasBuffer();
		mCanvas->AttachBufferInitPainter(mCanvasBuffer);
	}

	void QtEngineWindow::ReinitCanvasBuffer()
	{
		if (mCanvasBuffer)
		{
			delete mCanvasBuffer;
		}
		mCanvasBuffer = new QPixmap( QMainWindow::size() );
	}

	void QtEngineWindow::ConnectActions()
	{		
		connect( ui.actionNewGameWithPC, SIGNAL( triggered() ), SLOT( OnNewGameWithPC() ) );
		connect( ui.actionAbout, SIGNAL( triggered() ), SLOT( OnAbout() ) );
	}

	void QtEngineWindow::OnNewGameWithPC()
	{
		QMessageBox::information(NULL, "Hello!", "");
	}

	void QtEngineWindow::OnAbout()
	{
		QMessageBox::information(NULL, "About", "Eleks C++ Academy 2013\nBattleship\nBohdan Popyk");
	}
}
