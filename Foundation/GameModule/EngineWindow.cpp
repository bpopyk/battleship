#include "PrecompiledHeader.h"

#include <cassert>

#include "EngineWindow.h"

namespace Battleship
{
	EngineWindow::EngineWindow() : mPaintingNow(false)
	{
	}

	EngineWindow::~EngineWindow()
	{
	}

	void EngineWindow::BeginPaint()
	{
		assert( !mPaintingNow );
		DoBeginPaint();
		mPaintingNow = true;
	}

	void EngineWindow::EndPaint()
	{
		assert(mPaintingNow);
		DoEndPaint();
		mPaintingNow = false;
	}

	Canvas& EngineWindow::GetCanvas()
	{
		return DoGetCanvas();
	}
}