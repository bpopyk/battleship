#ifndef BS_GRID_H
#define BS_GRID_H

#include <vector>

#define BS_GRID_DIMENSION 10U

namespace Battleship
{
	typedef std::vector<Cell> CellsContainer;
	typedef std::vector<Cell>::iterator CellsContainerIterator;

	class Grid
	{
	public:
		Grid() : mCells(BS_GRID_DIMENSION * BS_GRID_DIMENSION) {}

		const Cell& CellAt(size_t offset) const;
		Cell& CellAt(size_t offset);
		const Cell& CellAt(size_t row, size_t column) const;
		Cell& CellAt(size_t row, size_t column);

	private:
		CellsContainer mCells;
	};
}

#endif // BS_GRID_H