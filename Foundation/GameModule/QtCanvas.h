#ifndef BS_QT_CANVAS_H
#define BS_QT_CANVAS_H

#include <QtWidgets/QWidget>
#include <QtGui/QPixmap>
#include <QtGui/QPainter>
#include <QtGui/QPen.h>
#include <QtGui/QBrush>

#include "Canvas.h"

namespace Battleship
{
	class QtCanvas : public Canvas
	{
	public:
		QtCanvas(QWidget* canvasWidget);
		virtual ~QtCanvas();

		void AttachBufferInitPainter(QPixmap* buffer);
		void DetachBufferFinalizePainter();

	private:
		virtual void DoSetBrushColor(int r, int g, int b);
		virtual void DoSetPenColor(int r, int g, int b);
		virtual void DoDrawLine(int x1, int y1, int x2, int y2);
		virtual void DoDrawRect(int x, int y, int width, int height);
		virtual void DoDrawX(int x, int y, int width, int height);
		virtual void DoBeginPaint();
		virtual void DoEndPaint();

	private:
		QWidget* mCanvasWidget;
		QPixmap* mBuffer;
		QPainter* mPainter;
		QPen mPen;
		QBrush mBrush;
	};
}

#endif // BS_QT_CANVAS_H