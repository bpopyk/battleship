#ifndef BS_ENGINE_WINDOW_H
#define BS_ENGINE_WINDOW_H

#include "Canvas.h"

namespace Battleship
{
	class EngineWindow
	{
	public:
		EngineWindow();
		virtual ~EngineWindow();

		void BeginPaint();
		void EndPaint();
		Canvas& GetCanvas();

	private:
		virtual void DoBeginPaint() = 0;
		virtual void DoEndPaint() = 0;
		virtual Canvas& DoGetCanvas() = 0;

	private:
		bool mPaintingNow;
	};
}

#endif // BS_ENGINE_WINDOW_H
