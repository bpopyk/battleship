#include "PrecompiledHeader.h"
#include "Ship.h"

void Battleship::Ship::AddDeck(Cell *cell)
{
	if (cell)
	{
		assert( cell->IsShip() );
		mDecks.push_back(cell);
	}
}

const Battleship::Cell* Battleship::Ship::CellAt(size_t offset) const
{
	return mDecks.at(offset);
}