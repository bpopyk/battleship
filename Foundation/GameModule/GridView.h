#ifndef BS_GRID_VIEW_H
#define BS_GRID_VIEW_H

#include "Grid.h"
#include "Canvas.h"

namespace Battleship
{
	typedef unsigned char Byte;

	class Rectangle
	{
	public:
		Rectangle();
		Rectangle(int x, int y, int width, int height);
		Rectangle(const Rectangle& rectangle);
		Rectangle& operator=(const Rectangle& rectangle);
		int GetX();
		int GetY();
		int GetWidth();
		int GetHeight();

	private:
		int mX, mY, mWidth, mHeight;
	};

	class GridView
	{
	public:
		GridView();
		GridView(const Rectangle& rectangle);
		virtual ~GridView() {}

		void RenderGrid(const Grid& grid, Canvas& canvas);

	private:
		void RenderGridBase(const Grid& grid, Canvas& canvas);

	private:
		Rectangle mGridRectangle;
	};
}

#endif // BS_GRID_VIEW_H