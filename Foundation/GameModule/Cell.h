#ifndef BS_CELL_H
#define BS_CELL_H

namespace Battleship
{
	typedef unsigned int CellState;

	class Cell
	{
	public:
		Cell() : mState(0) {}

		bool IsEmpty() const;					// Checks for absence ship part and !!!shoot
		bool IsShip() const;
		bool IsShot() const;
		bool IsMultiDeck() const;

		void PutOneDeckPart();
		void PutMultiDeckPart();
		void MakeShoot();

		static Cell GetEmptyCell();

	private:
		enum StateFlags
		{
			StateFlags_Ship			= 0x01,		// Whether there is part of ship (1) or empty (0)
			StateFlags_Shot			= 0x02,		// Whether was shoot (1) or not (0)
			StateFlags_MultiDeck	= 0x04		// Wherher is part of multi-deck (1) or one-deck ship (0)
		};

		//static const CellState 

	private:
		CellState mState;
	};
}

#endif // BS_CELL_H