#ifndef BS_GAME_H
#define BS_GAME_H

#include "Player.h"

namespace Battleship
{
	class Game
	{
	public:
		Game() {}
		virtual ~Game() {}

	private:
		Player mPlayer1, mPlayer2;
	};
}

#endif // BS_GAME_H