#include "PrecompiledHeader.h"
#include "QtEngine.h"

namespace Battleship
{
	QtEngine::QtEngine( int argc, char *argv[] ) :
		mApplication( new QApplication(argc, argv) ),
		mWindow( new QtEngineWindow() ),
		mTimer( new QTimer() )
	{
	}

	QtEngine::~QtEngine()
	{
		if (mApplication)
		{
			delete mApplication;
		}
		if (mWindow)
		{
			delete mWindow;
		}
		if (mTimer)
		{
			delete mTimer;
		}
	}

	void QtEngine::DoRun()
	{
		assert(mWindow != nullptr);
		assert(mTimer != nullptr);
		mWindow->show();
		mTimer->start();
		mApplication->exec();
	}

	EngineWindow& QtEngine::DoInitWindow()
	{
		return *mWindow;
	}
}