#include "PrecompiledHeader.h"
#include "Grid.h"

const Battleship::Cell& Battleship::Grid::CellAt(size_t offset) const
{
	return mCells.at(offset);
}

Battleship::Cell& Battleship::Grid::CellAt(size_t offset)
{
	return mCells.at(offset);
}

const Battleship::Cell& Battleship::Grid::CellAt(size_t row, size_t column) const
{
	return CellAt(row * BS_GRID_DIMENSION + column);
}

Battleship::Cell& Battleship::Grid::CellAt(size_t row, size_t column)
{
	return CellAt(row * BS_GRID_DIMENSION + column);
}