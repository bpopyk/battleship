/********************************************************************************
** Form generated from reading UI file 'GameModule.ui'
**
** Created by: Qt User Interface Compiler version 5.0.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAMEMODULE_H
#define UI_GAMEMODULE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GameModuleClass
{
public:
    QAction *actionNewGameWithPC;
    QAction *actionAbout;
    QWidget *centralWidget;
    QMenuBar *menuBar;
    QMenu *menuGame;
    QMenu *menuHelp;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *GameModuleClass)
    {
        if (GameModuleClass->objectName().isEmpty())
            GameModuleClass->setObjectName(QStringLiteral("GameModuleClass"));
        GameModuleClass->setEnabled(true);
        GameModuleClass->resize(800, 600);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(GameModuleClass->sizePolicy().hasHeightForWidth());
        GameModuleClass->setSizePolicy(sizePolicy);
        GameModuleClass->setMinimumSize(QSize(800, 600));
        GameModuleClass->setMaximumSize(QSize(800, 600));
        actionNewGameWithPC = new QAction(GameModuleClass);
        actionNewGameWithPC->setObjectName(QStringLiteral("actionNewGameWithPC"));
        actionAbout = new QAction(GameModuleClass);
        actionAbout->setObjectName(QStringLiteral("actionAbout"));
        centralWidget = new QWidget(GameModuleClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        GameModuleClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(GameModuleClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 800, 21));
        menuGame = new QMenu(menuBar);
        menuGame->setObjectName(QStringLiteral("menuGame"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QStringLiteral("menuHelp"));
        GameModuleClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(GameModuleClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        GameModuleClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(GameModuleClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setSizeGripEnabled(false);
        GameModuleClass->setStatusBar(statusBar);

        menuBar->addAction(menuGame->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuGame->addAction(actionNewGameWithPC);
        menuHelp->addAction(actionAbout);

        retranslateUi(GameModuleClass);

        QMetaObject::connectSlotsByName(GameModuleClass);
    } // setupUi

    void retranslateUi(QMainWindow *GameModuleClass)
    {
        GameModuleClass->setWindowTitle(QApplication::translate("GameModuleClass", "GameModule", 0));
        actionNewGameWithPC->setText(QApplication::translate("GameModuleClass", "&New game with PC", 0));
        actionAbout->setText(QApplication::translate("GameModuleClass", "&About", 0));
        menuGame->setTitle(QApplication::translate("GameModuleClass", "&Game", 0));
        menuHelp->setTitle(QApplication::translate("GameModuleClass", "&Help", 0));
    } // retranslateUi

};

namespace Ui {
    class GameModuleClass: public Ui_GameModuleClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAMEMODULE_H
