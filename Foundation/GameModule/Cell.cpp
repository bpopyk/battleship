#include "PrecompiledHeader.h"
#include "Cell.h"

bool Battleship::Cell::IsEmpty() const
{
	bool result;
	result = mState == !StateFlags_Ship;
	return result;
}

bool Battleship::Cell::IsShip() const
{
	bool result;
	result = (mState & StateFlags_Ship) != 0;
	return result;
}

bool Battleship::Cell::IsShot() const
{
	bool result;
	result = (mState & StateFlags_Shot) != 0;
	return result;
}

bool Battleship::Cell::IsMultiDeck() const
{
	bool result;
	result = IsShip() && (mState & StateFlags_MultiDeck) != 0;
	return result;
}

void Battleship::Cell::PutOneDeckPart()
{
	assert( IsEmpty() );
	mState |= StateFlags_Ship;
}

void Battleship::Cell::PutMultiDeckPart()
{
	assert( IsEmpty() );
	mState |= StateFlags_Ship | StateFlags_MultiDeck;
}

void Battleship::Cell::MakeShoot()
{
	assert( !IsShot() );
	mState |= StateFlags_Shot;
}

Battleship::Cell Battleship::Cell::GetEmptyCell()
{
	return	Cell();
}