#ifndef BS_PRECOMPILED_HEADER_H
#define BS_PRECOMPILED_HEADER_H

#include <cassert>
#include <vector>

#include <QtWidgets/QApplication>
#include <QMessageBox>

#include "Cell.h"
#include "Ship.h"
#include "Grid.h"

#endif // BS_PRECOMPILED_HEADER_H