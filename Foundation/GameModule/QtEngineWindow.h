#ifndef BS_GAMEMODULE_H
#define BS_GAMEMODULE_H

#include <QtWidgets/QMainWindow>
#include <QtGui/QPixmap>
#include <QAction>

#include "ui_GameModule.h"

#include "QtCanvas.h"
#include "EngineWindow.h"

namespace Battleship
{
	class QtEngineWindow : public QMainWindow, public EngineWindow
	{
		Q_OBJECT

	public:
		QtEngineWindow(QWidget *parent = 0);
		~QtEngineWindow();

	private:
		virtual void DoBeginPaint();
		virtual void DoEndPaint();
		virtual Canvas& DoGetCanvas();

	private:
		void ReinitCanvas();
		void ReinitCanvasBuffer();
		void ConnectActions();

	private slots:
		void OnNewGameWithPC();
		void OnAbout();

	private:
		QtCanvas* mCanvas;
		QPixmap* mCanvasBuffer;
		bool mPaintingNow;

		Ui::GameModuleClass ui;
	};
}

#endif // BS_GAMEMODULE_H
