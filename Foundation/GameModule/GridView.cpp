#include "PrecompiledHeader.h"
#include "GridView.h"

namespace Battleship
{
	Rectangle::Rectangle() :
		mX(0),
		mY(0),
		mWidth(0),
		mHeight(0)
	{
	}

	Rectangle::Rectangle(int x, int y, int width, int height) :
		mX(x),
		mY(y),
		mWidth(width),
		mHeight(height)
	{
	}

	Rectangle::Rectangle(const Rectangle& rectangle)
	{
		if (this != &rectangle)
		{
			mX = rectangle.mX;
			mY = rectangle.mY;
			mWidth = rectangle.mWidth;
			mHeight = rectangle.mHeight;
		}
	}

	Rectangle& Rectangle::operator=(const Rectangle& rectangle)
	{
		if (this != &rectangle)
		{
			mX = rectangle.mX;
			mY = rectangle.mY;
			mWidth = rectangle.mWidth;
			mHeight = rectangle.mHeight;
		}
		return *this;
	}

	int Rectangle::GetX()
	{
		return mX;
	}

	int Rectangle::GetY()
	{
		return mY;
	}

	int Rectangle::GetWidth()
	{
		return mWidth;
	}

	int Rectangle::GetHeight()
	{
		return mHeight;
	}

	GridView::GridView() : mGridRectangle()
	{
	}

	GridView::GridView(const Rectangle& rectangle) : mGridRectangle(rectangle)
	{
	}

	void GridView::RenderGrid(const Grid& grid, Canvas& canvas)
	{
		RenderGridBase(grid, canvas);
		//...
	}

	void GridView::RenderGridBase(const Grid& grid, Canvas& canvas)
	{
		canvas.DrawRect( mGridRectangle.GetX(), mGridRectangle.GetY(), mGridRectangle.GetWidth(), mGridRectangle.GetHeight() );
		int cellWidth = mGridRectangle.GetWidth() / BS_GRID_DIMENSION;
		int cellHeight = mGridRectangle.GetHeight() / BS_GRID_DIMENSION;
		for (Byte i = 1; i < BS_GRID_DIMENSION; ++i)
		{
			int x = mGridRectangle.GetX() + i * cellWidth;
			canvas.DrawLine( x, mGridRectangle.GetY(), x, mGridRectangle.GetY() + mGridRectangle.GetHeight() );
			int y = mGridRectangle.GetY() + i * cellHeight;
			canvas.DrawLine(mGridRectangle.GetX(), y, mGridRectangle.GetX() + mGridRectangle.GetWidth(), y);
		}
	}
}