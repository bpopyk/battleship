#ifndef BS_QT_ENGINE_H
#define BS_QT_ENGINE_H

#include <QtWidgets/QApplication>
#include <QtCore/QTimer>

#include "Engine.h"
#include "QtEngineWindow.h"

namespace Battleship
{
	class QtEngine : public QObject, public Engine
	{
		Q_OBJECT
	public:
		QtEngine( int argc, char *argv[] );
		virtual ~QtEngine();

	private:
		virtual void DoRun();
		virtual EngineWindow& DoInitWindow();

	private:
		QApplication* mApplication;
		QtEngineWindow* mWindow;
		QTimer* mTimer;
	};
}

#endif // BS_QT_ENGINE_H