#ifndef BS_ENGINE_H
#define BS_ENGINE_H

#include "Game.h"
#include "EngineWindow.h"

namespace Battleship
{
	class Engine
	{
	public:
		Engine() {}
		virtual ~Engine() {}

		void Run(Game* game);
		EngineWindow& InitWindow();

	private:
		virtual void DoRun() = 0;
		virtual EngineWindow& DoInitWindow() = 0;

	private:
		Game* mGame;
	};
}

#endif // BS_ENGINE_H