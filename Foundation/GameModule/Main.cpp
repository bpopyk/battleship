#include "PrecompiledHeader.h"

#include "QtEngine.h"
#include "QtEngineWindow.h"


int main(int argc, char *argv[])
{
	//Battleship::Engine *engine = new Battleship::QtEngine(argc, argv);
	//Battleship::EngineWindow *window = &engine->InitWindow();
	//Battleship::Game *game = new Battleship::Game();
	//engine->Run(game);


	QApplication a(argc, argv);
	Battleship::QtEngineWindow w;
	w.show();
	return a.exec();
}