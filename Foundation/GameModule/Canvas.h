#ifndef BS_CANVAS_H
#define BS_CANVAS_H

namespace Battleship
{
	class Canvas
	{
	public:
		virtual ~Canvas() {}

		void SetPenColor(int r, int g, int b);
		void SetBrushColor(int r, int g, int b);
		void DrawLine(int x1, int y1, int x2, int y2);
		void DrawRect(int x, int y, int width, int height);
		void DrawX(int x1, int y1, int width, int height);			//Draws 2 intersecting lines
		void BeginPaint();
		void EndPaint();

	private:
		virtual void DoSetBrushColor(int r, int g, int b) = 0;
		virtual void DoSetPenColor(int r, int g, int b) = 0;
		virtual void DoDrawLine(int x1, int y1, int x2, int y2) = 0;
		virtual void DoDrawRect(int x, int y, int width, int height) = 0;
		virtual void DoDrawX(int x1, int y1, int width, int height) = 0;
		virtual void DoBeginPaint() = 0;
		virtual void DoEndPaint() = 0;
	};
}

#endif // BS_CANVAS_H